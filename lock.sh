#!/bin/bash

# Set the current directory to the script's current directory if it's not already done
if [ "$SCRIPT_PATH" != $(echo "$PWD"/"${0%/*}" | sed 's/\/\.//') ] ; then
	SCRIPT_PATH=$(echo "$PWD"/"${0%/*}" | sed 's/\/\.//')
	if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ] ; then
		cd $SCRIPT_PATH
	fi
fi

# calling the commit script before locking
./commit.sh
echo -e "\n"

echo -e "\e[1;31mLocking vault...\e[0m"
cryfs-unmount "$SCRIPT_PATH/entries"
echo -e "\e[1;32mVault locked!\e[0m"
