#!/bin/bash

# Set the current directory to the script's current directory if it's not already done
if [ "$SCRIPT_PATH" != $(echo "$PWD"/"${0%/*}" | sed 's/\/\.//') ] ; then
	SCRIPT_PATH=$(echo "$PWD"/"${0%/*}" | sed 's/\/\.//')
	if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ] ; then
		cd $SCRIPT_PATH
	fi
fi

if [ -d "$SCRIPT_PATH/.encrypteddata" ] && [ -d "$SCRIPT_PATH/entries" ] ; then
	echo -e "\e[0;32mNew entry nammed :\e[0m"
	if [ -n "$1" ]; then
		echo -e "$SCRIPT_PATH/entries/$1/$(date +%F-%a-%R).md\n"
		mkdir -p "$SCRIPT_PATH/entries/$1"
		touch "$SCRIPT_PATH/entries/$1/$(date +%F-%a-%R).md"
	else
		echo -e "$SCRIPT_PATH/entries/$(date +%F-%a-%R).md\n"
		mkdir -p "$SCRIPT_PATH/entries"
		touch "$SCRIPT_PATH/entries/$(date +%F-%a-%R).md"
	fi
else
	echo -e "\e[41mERROR :\e[0m \e[1;31mNo vault created or accessible.\e[0m"
	echo -e "\e[0;36mHINT : did you try ./unlock.sh first?\e[0m"
fi