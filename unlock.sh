#!/bin/bash

# Set the current directory to the script's current directory if it's not already done
if [ "$SCRIPT_PATH" != $(echo "$PWD"/"${0%/*}" | sed 's/\/\.//') ] ; then
	SCRIPT_PATH=$(echo "$PWD"/"${0%/*}" | sed 's/\/\.//')
	if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ] ; then
		cd $SCRIPT_PATH
	fi
fi

echo -e "\e[1;33mUnlocking vault...\e[0m"
mkdir -p "$SCRIPT_PATH/.encrypteddata" "$SCRIPT_PATH/entries"
cryfs "$SCRIPT_PATH/.encrypteddata" "$SCRIPT_PATH/entries"
echo -e "\e[1;31mVault unlocked!\e[0m"