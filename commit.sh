#!/bin/bash

# Set the current directory to the script's current directory if it's not already done
if [ "$SCRIPT_PATH" != $(echo "$PWD"/"${0%/*}" | sed 's/\/\.//') ] ; then
	SCRIPT_PATH=$(echo "$PWD"/"${0%/*}" | sed 's/\/\.//')
	if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ] ; then
		cd $SCRIPT_PATH
	fi
fi

# TODO : need to add something in case there's nothing to commit
echo -e "\e[1;31mCommiting changes...\e[0m"
echo -e "\e[1;31mDon't forget to lock your vault! \e[0m \n"
if [ -n "$1" ]; then
	git add . && git commit -m"$(date +%F-%a-%R) $(echo "$@")"
else
	git add . && git commit -m"$(date +%F-%a-%R) no message"
fi