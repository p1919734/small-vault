# Small Vault (small-vault)

### Small vault using CryFS and others Linux command.

### Lightweight and fast, easy to configure and modify.

## Why do you want to use Small Vault?

Small Vault was created to be used to keep a personal journal and save it on
some git server (github/gitlab and such). Although forking this project and
using it for yourself *should* be safe, it should be noted that only the files
already in the project and the `.encrypteddata` directory should be uploaded
anywhere. The `entries` directory **SHOULD NOT** be logged by git (that is why
this file is in the `.gitignore`).

## Scripts :

* `unlock.sh` : open (and create if it`s not already done) the vault.
* `new-entry.sh` : create a new journal entry. By default it will be located in
  the `entries` directory, but you can specify a new directory inside `entries`
  when using the script.
* `commit.sh` : add the changes made to your git. You can specify a commit
  message that will be appended to the date of the commit in the commit
  message.
* `lock.sh` : add the changes to your git then lock the vault.

## Version :

Early version, some feature may be added in the future.
